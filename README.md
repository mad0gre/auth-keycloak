# Auth - Keycloak


## Introduction

The purpose of this project is to show a complete example of authentication and authorization using Keycloak. It
consists of three parts:

1) Authorization Server (Keycloak): The authorization server issues tokens to authenticated client applications.  
2) Client Application: An application willing to access protected resources on the resource server.  
3) Resource Server: A server that manages access to protected resources, allowing access based on Access Tokens.  

For the scope of this example project, the authorization will use the OpenID Connect 1.0 Implicit Flow (`response_type 
= token`). See all available OpenID Connect 1.0 flows [here][OpenId Connect Flows]: 


### Requirements

Project was created with the following:
- Docker 18.09
- Maven 3.6.0
- JDK 8
- Node.js 10.15.3
- npm 6.9.0
- Angular 7.3.8


### How to Build

- Run `maven clean install` to build the project.
- Run `docker compose up --build` on the root folder of the project. It requires the following environment variables:
    - `KEYCLOAK_USER = admin` (Username of the admin account for Keycloak)
    - `KEYCLOAK_PASSWORD = pass` (Password of the admin account for Keycloak)
    - `MP_JWT_VERIFY_PUBLICKEY_LOCATION = 
      http://keycloak-server:8080/auth/realms/example/protocol/openid-connect/certs` (URL used by the resource server 
      to retrieve Keycloak's public key to validate JWT).
    - `MP_JWT_VERIFY_ISSUER = http://localhost:8180/auth/realms/example` (Issuer of the token - value present on the iss 
      claim of the JWT, used by the resource server during JWT validation).
    - `APP_AUTH_ISSUER = http://localhost:8180/auth/realms/example` (Issuer of the token - value present on the iss 
      claim of the JWT, used by the resource server during JWT validation)
    - `APP_AUTH_CLIENT = rng_app` (Client ID used to authenticate on the Authorization Server)
    - `APP_RESOURCE_SERVER = http://localhost:8280/resource-server/api/rng` (URL of the rng endpoint of the Resource 
      Server)

The example values used above are suitable to run the project on the local environment. 

## Authorization Server

The authorization server of this project is the keycloak-server module. It extends the official `jboss/keycloak` image 
and relies on an in-memory H2 database to store the configuration - that is preloaded during startup by importing the 
`keycloak-server/realm/example-realm-export.json` file.

The preloaded configuration of Keycloak adds the "Example" realm with a few users already set up to quickly test this
project:
- all@user.test: User that belongs to all groups - can access the rng endpoint of the resource server successfully.
- dummy@user.test User belongs to dummy group - cannot access the rng endpoint of the resource server.
- rng@user.test: User belongs to rng group - can access the rng endpoint of the resource server successfully.

All users have the same password: test.

The preloaded configuration also creates a rng_app client used on this example project. This client is properly 
configured so that the tokens adhere to Microprofile specification.

To test solely the response server, a JWT can be generated for a user using the password grant type:
```
POST http://localhost:8180/auth/realms/example/protocol/openid-connect/token
Accept: */*
Cache-Control: no-cache
Content-Type: application/x-www-form-urlencoded

grant_type=password&username=all@user.test&password=test&client_id=rng_app
```


## Resource Server

The resource server is a JEE application running on top of Payara Micro exposing a single endpoint: 
`resource-server/api/rng`. This endpoint is secured using the Microprofile-JWT spec, and a valid token must be sent on 
the Authorization header using the bearer schema. An example call to this endpoint:
```
GET http://localhost:8280/resource-server/api/rng
Accept: */*
Cache-Control: no-cache
Authorization: Bearer <REPLACE WITH JWT TOKEN>
```

If the above call is successful, a response with HTTP Status 200 (OK) is given with a response body containing a random 
number. If the token is invalid, a response with HTTP Status 401 (Unauthorized) is given. If the token is valid but the user does not
have the required role (groups claim), a response with HTTP Status 403 (Forbidden) is given.


## Client Application

The client application is a bare-bones single-page Angular application deployed on nginx. OpenID Connect Implicit Flow
is initiated against the Authentication Server. It retrieves the ID token and uses it to call the RNG endpoint for 
authentication when the rng button is pressed.

To access the client application, just go to `http://localhost:8080/` in any browser.


[OpenId Connect Flows]: https://medium.com/@darutk/diagrams-of-all-the-openid-connect-flows-6968e3990660