package mad0gre.rng.controller;

import mad0gre.rng.service.RNGService;
import org.eclipse.microprofile.jwt.JsonWebToken;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.ws.rs.core.Response;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class RNGControllerTest {

    @Mock
    private RNGService rngService;

    @Mock
    private JsonWebToken jsonWebToken;

    @InjectMocks
    private RNGController rngController;

    @Test
    void generate_minHigherThanMax_badRequest() {
        Response response = rngController.generate(10, 9);
        Map<String, String> error = (Map<String, String>) response.getEntity();

        assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), response.getStatus());
        assertTrue(error.containsKey("error"));
        assertEquals("Min must be lesser than max.", error.get("error"));
    }

    @Test
    void generate_minEqualsMax_badRequest() {
        Response response = rngController.generate(171, 171);
        Map<String, String> error = (Map<String, String>) response.getEntity();

        assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), response.getStatus());
        assertTrue(error.containsKey("error"));
        assertEquals("Min must be lesser than max.", error.get("error"));
    }

    @Test
    void generate_minLessThanMax_ok() {
        long min = 1L;
        long max = 1000L;
        long res = 666L;
        when(rngService.generate(min, max)).thenReturn(res);

        Response response = rngController.generate(min, max);
        Map<String, String> error = (Map<String, String>) response.getEntity();

        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
        assertTrue(error.containsKey("value"));
        assertEquals(res, error.get("value"));
    }
}
