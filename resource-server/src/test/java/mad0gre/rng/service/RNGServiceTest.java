package mad0gre.rng.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.function.Executable;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.security.SecureRandom;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class RNGServiceTest {

    @Mock
    private SecureRandom secureRandom;

    @InjectMocks
    private RNGService rngService;

    @Test
    void generate_minHigherThanMax_exception() {
        Executable executable = () -> rngService.generate(10, 9);

        Exception e = assertThrows(IllegalArgumentException.class, executable);
        assertEquals("Min must be lesser than max.", e.getMessage());
    }

    @Test
    void generate_minEqualsMax_exception() {
        Executable executable = () -> rngService.generate(11111, 11111);

        Exception e = assertThrows(IllegalArgumentException.class, executable);
        assertEquals("Min must be lesser than max.", e.getMessage());
    }

    @Test
    void generate_minLessThanZero_success() {
        when(secureRandom.nextLong()).thenReturn(98L);

        long result = rngService.generate(-5, 6);

        assertEquals(5L, result);
    }

    @Test
    void generate_minEqualZero_success() {
        when(secureRandom.nextLong()).thenReturn(512L);

        long result = rngService.generate(0, 256);

        assertEquals(0L, result);
    }

    @Test
    void generate_minHigherThanZero_success() {
        when(secureRandom.nextLong()).thenReturn(4096L);

        long result = rngService.generate(1000, 2000);

        assertEquals(1096L, result);
    }

    @Test
    void generate_maxLessThanZero_success() {
        when(secureRandom.nextLong()).thenReturn(2L);

        long result = rngService.generate(-10, -4);

        assertEquals(-8L, result);
    }
}
