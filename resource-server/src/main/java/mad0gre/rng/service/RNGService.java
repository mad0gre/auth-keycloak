package mad0gre.rng.service;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.security.SecureRandom;

/**
 * Service dealing with random number generation. Generates a new random value based on a
 * {@link java.security.SecureRandom} instance.
 */
@ApplicationScoped
public class RNGService {

    @Inject
    private SecureRandom secureRandom;

    /**
     * Generates a new random long value in between min and max. Throws an {@link IllegalArgumentException} if min is
     * not lesser than max.
     *
     * @param min Minimum value to be generated (inclusive).
     * @param max Maximum value to be generated (exclusive).
     * @return A new random number.
     */
    public long generate(long min, long max) {
        if (min >= max) {
            throw new IllegalArgumentException("Min must be lesser than max.");
        }

        long adjustedMax = max - min;
        return Math.abs(secureRandom.nextLong() % adjustedMax) + min;
    }
}
