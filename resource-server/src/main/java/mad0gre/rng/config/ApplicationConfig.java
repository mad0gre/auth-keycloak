package mad0gre.rng.config;

import org.eclipse.microprofile.auth.LoginConfig;

import javax.annotation.security.DeclareRoles;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.security.SecureRandom;

/**
 * Application configuration. JAX-RS components are automatically scanned.
 */
@ApplicationPath("api")
@ApplicationScoped
@LoginConfig(authMethod = "MP-JWT")
@DeclareRoles({"rng"})
public class ApplicationConfig extends Application {

    /**
     * {@link SecureRandom} producer for CDI injection.
     *
     * @return {@link SecureRandom} object to be injected.
     */
    @Produces
    public SecureRandom produceSecureRandom() {
        return new SecureRandom();
    }
}
