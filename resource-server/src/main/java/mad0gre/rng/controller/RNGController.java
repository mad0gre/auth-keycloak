package mad0gre.rng.controller;

import lombok.extern.slf4j.Slf4j;
import mad0gre.rng.service.RNGService;
import org.eclipse.microprofile.jwt.JsonWebToken;

import javax.annotation.security.DenyAll;
import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Collections;
import java.util.Map;

/**
 * Controller containing RNG endpoints.
 */
@Path("rng")
@DenyAll
@RequestScoped
@Slf4j
public class RNGController {

    @Inject
    private RNGService rngService;

    @Inject
    private JsonWebToken jsonWebToken;

    /**
     * Random Number Generator (RNG) endpoint. Generates a random long value between min and max.
     *
     * @param min Minimum value to be generated (inclusive).
     * @param max Maximum value to be generated (exclusive).
     * @return {@link Response} object containing the random number generated.
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed({"rng"})
    public Response generate(
            @QueryParam("min") @DefaultValue("0") long min,
            @QueryParam("max") @DefaultValue("100") long max) {

        log.info("JWT: {}.", jsonWebToken.getRawToken());

        if (min >= max) {
            Map<String, String> error = Collections.singletonMap("error", "Min must be lesser than max.");
            return Response.status(Response.Status.BAD_REQUEST).entity(error).build();
        }

        long value = rngService.generate(min, max);

        return Response
                .ok(Collections.singletonMap("value", value))
                .build();
    }
}
