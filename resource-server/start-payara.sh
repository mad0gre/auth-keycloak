#!/usr/bin/env bash

java \
-Dmp.jwt.verify.publickey.location=${MP_JWT_VERIFY_PUBLICKEY_LOCATION} \
-Dmp.jwt.verify.issuer=${MP_JWT_VERIFY_ISSUER} \
-jar /opt/payara/payara-micro.jar \
--deploymentDir /opt/payara/deployments \
--noCluster \
--port 8080