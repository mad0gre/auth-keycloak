import {AuthConfig} from 'angular-oauth2-oidc';

import {environment} from "../environments/environment";

export const authConfig: AuthConfig = {

  // Url of the Identity Provider.
  issuer: environment.authIssuer,

  // URL to redirect the user to after login.
  redirectUri: window.location.origin + '/index.html',

  // The client application id.
  clientId: environment.authClient,

  // Set the scope for the permissions the client should request.
  scope: 'openid profile email',

  // Remove the requirement of using HTTPS to simplify the demo.
  requireHttps: false,

  // Change this to false to get only id token. It seems to break validation, however.
  requestAccessToken: true
}
