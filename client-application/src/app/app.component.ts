import {Component} from '@angular/core';

import {Headers, Http} from '@angular/http';

import {JwksValidationHandler, OAuthService} from 'angular-oauth2-oidc';

import {authConfig} from './auth.config';
import {environment} from "../environments/environment";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'client-application';
  rngValue: number;

  constructor(private oauthService: OAuthService, private http: Http) {
    this.configureWithNewConfigApi();
  }

  private configureWithNewConfigApi() {
    this.oauthService.configure(authConfig);
    this.oauthService.tokenValidationHandler = new JwksValidationHandler();
    this.oauthService.loadDiscoveryDocumentAndTryLogin();
  }

  public login() {
    this.oauthService.initImplicitFlow();
  }

  public logoff() {
    this.oauthService.logOut();
  }

  public rng() {
    let headers = new Headers({
      'Authorization': `Bearer ${this.idToken}`
    });

    this.http.get(
      environment.resourceServer,
      {headers: headers}
    ).subscribe(res => this.rngValue = res.json().value);
  }

  public get name() {
    let claims = this.oauthService.getIdentityClaims();
    if (!claims) return null;
    return claims['given_name'];
  }

  public get idToken() {
    return this.oauthService.getIdToken();
  }

  public get rngResult() {
    return this.rngValue;
  }
}
