export const environment = {
  production: true,
  authIssuer: $ENV.AUTH_ISSUER,
  authClient: $ENV.AUTH_CLIENT,
  resourceServer: $ENV.RESOURCE_SERVER
};
