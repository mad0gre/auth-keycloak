declare var $ENV: Env;

interface Env {
  AUTH_ISSUER: string;
  AUTH_CLIENT: string;
  RESOURCE_SERVER: string;
}
