const webpack = require('webpack');

module.exports = {
  plugins: [
    new webpack.DefinePlugin({
      $ENV: {
        AUTH_ISSUER: JSON.stringify(process.env.APP_AUTH_ISSUER),
        AUTH_CLIENT: JSON.stringify(process.env.APP_AUTH_CLIENT),
        RESOURCE_SERVER: JSON.stringify(process.env.APP_RESOURCE_SERVER)
      }
    })
  ]
};
